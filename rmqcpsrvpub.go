package rmqcpsrvpubsub

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
)

type RmqCpsrvPub struct {
	AmpqUrl string
	conn    *amqp.Connection
	ch      *amqp.Channel
}

func (pub *RmqCpsrvPub) Start() {
	var err error

	pub.conn, err = amqp.Dial(pub.AmpqUrl)
	if err != nil {
		log.Print(err)
	}

	pub.ch, err = pub.conn.Channel()
	if err != nil {
		log.Print(err)
	}
}

func (pub *RmqCpsrvPub) Publish(request XFMRequest) (err error) {
	bytesrequest, _ := json.Marshal(request)
	exchangeName := request.Protocol + ":XFMRequest"

	err = pub.ch.Publish(
		exchangeName,
		request.Protocol,
		false,
		false,
		amqp.Publishing{
			ContentType:  "text/json",
			Body:         bytesrequest,
			DeliveryMode: 2,
			Type:         request.RequestType,
		},
	)

	return err
}
