package rmqcpsrvpubsub

import (
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"log"
	"sycada/masstransitgo"
)

type RmqCpsrvSub struct {
	AMQPUrl      string
	SubscriberID string
	QueueName    string

	resetRequestHandler                 ResetRequestHandler
	remoteStopTransactionRequestHandler RemoteStopTransactionRequestHandler
	unlockConnectorRequestHandler       UnlockConnectorRequestHandler
	clearCacheRequestHandler            ClearCacheRequestHandler
	getDiagnosticsRequestHandler        GetDiagnosticsRequestHandler
	changeAvailabilityRequestHandler    ChangeAvailabilityRequestHandler
	updateFirmwareRequestHandler        UpdateFirmwareRequestHandler
	removeDiagnosticsRequestHandler     RemoveDiagnosticsRequestHandler
	getLogEntryRequestHandler           GetLogEntryRequestHandler

	running  bool
	done     chan bool
	conn     *amqp.Connection
	ch       *amqp.Channel
	messages <-chan amqp.Delivery
}

const (
	SoftResetMessageType             string = "SoftReset"
	HardResetMessageType             string = "HardReset"
	RemoteStopTransactionMessageType string = "RemoteStopTransaction"
	UnlockConnectorMessageType       string = "UnlockConnector"
	ClearCacheMessageType            string = "ClearCache"
	GetDiagnosticsMessageType        string = "GetDiagnostics"
	SetOperativeMessageType          string = "SetOperative"
	SetInoperativeMessageType        string = "SetInoperative"
	UpdateFirmwareMessageType        string = "UpdateFirmware"
	RemoveDiagnosticsMessageType     string = "RemoveDiagRequest"
	GetLogEntryMessageType           string = "GetLogEntry"
)

func NewRmqCpsrvSub(AMQPUrl string, subscriberId string, queueName string) *RmqCpsrvSub {
	return &RmqCpsrvSub{
		AMQPUrl:      AMQPUrl,
		SubscriberID: subscriberId,
		QueueName:    queueName,
		done:         make(chan bool),
	}
}

func (sub *RmqCpsrvSub) RegisterResetRequestHandler(handler ResetRequestHandler) {
	sub.resetRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterRemoteStopTransactionRequestHandler(handler RemoteStopTransactionRequestHandler) {
	sub.remoteStopTransactionRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterUnlockConnectorRequestHandler(handler UnlockConnectorRequestHandler) {
	sub.unlockConnectorRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterClearCacheRequestHandler(handler ClearCacheRequestHandler) {
	sub.clearCacheRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterGetDiagnosticsRequestHandler(handler GetDiagnosticsRequestHandler) {
	sub.getDiagnosticsRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterChangeAvailabilityRequestHandler(handler ChangeAvailabilityRequestHandler) {
	sub.changeAvailabilityRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterUpdateFirmwareRequestHandler(handler UpdateFirmwareRequestHandler) {
	sub.updateFirmwareRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterRemoveDiagnosticsRequestHandler(handler RemoveDiagnosticsRequestHandler) {
	sub.removeDiagnosticsRequestHandler = handler
}

func (sub *RmqCpsrvSub) RegisterGetLogEntryRequestHandler(handler GetLogEntryRequestHandler) {
	sub.getLogEntryRequestHandler = handler
}

var (
	ErrAlreadyStarted = errors.New("already started")
	ErrNotStarted     = errors.New("not started")
)

func (sub *RmqCpsrvSub) Start() error {
	if sub.running {
		return ErrAlreadyStarted
	}

	conn, err := amqp.Dial(sub.AMQPUrl)
	if err != nil {
		return errors.Wrapf(err, "cannot connect to %s", sub.AMQPUrl)
	}

	ch, err := conn.Channel()
	if err != nil {
		return errors.Wrapf(err, "cannot open channel to %s", sub.AMQPUrl)
	}

	messages, err := ch.Consume(
		sub.QueueName,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return errors.Wrapf(err, "cannot consume queue %s", sub.QueueName)
	}

	sub.running = true
	sub.conn = conn
	sub.ch = ch
	sub.messages = messages

	go sub.run()

	return nil
}

func (sub *RmqCpsrvSub) run() {
	for {
		select {
		case message := <-sub.messages:
			mtMessageParser := masstransitgo.NewMTMsgParser()
			if e := mtMessageParser.Parse(message.Body); e != nil {
				log.Printf("cannot parse the MT message %s\n", string(message.Body))
				sub.ch.Nack(message.DeliveryTag, false, true)
				return
			}

			if sub.resetRequestHandler != nil &&
				(mtMessageParser.PayloadMessageIsOfType(SoftResetMessageType) ||
					mtMessageParser.PayloadMessageIsOfType(HardResetMessageType)) {
				var resetReq ResetRequest

				if e := mtMessageParser.Unmarshal(&resetReq); e != nil {
					log.Printf("cannot unmarshall a ResetRequest from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.resetRequestHandler(&resetReq); e != nil {
					log.Println(errors.Wrapf(e, "error handling a ResetRequest msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.remoteStopTransactionRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(RemoteStopTransactionMessageType) {
				var stopTransactionRequest RemoteStopTransactionRequest

				if e := mtMessageParser.Unmarshal(&stopTransactionRequest); e != nil {
					log.Printf("cannot unmarshall a Stop Transaction Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.remoteStopTransactionRequestHandler(&stopTransactionRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling a Stop Transaction Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.unlockConnectorRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(UnlockConnectorMessageType) {
				var unlockConnectorRequest UnlockConnectorRequest

				if e := mtMessageParser.Unmarshal(&unlockConnectorRequest); e != nil {
					log.Printf("cannot unmarshall an Unlock Connector Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.unlockConnectorRequestHandler(&unlockConnectorRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling an Unlock Connector Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.clearCacheRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(ClearCacheMessageType) {
				var clearCacheRequest ClearCacheRequest

				if e := mtMessageParser.Unmarshal(&clearCacheRequest); e != nil {
					log.Printf("cannot unmarshall a Clear Cache Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.clearCacheRequestHandler(&clearCacheRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling a Clear Cache Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.getDiagnosticsRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(GetDiagnosticsMessageType) {
				var getDiagnosticsRequest GetDiagnosticsRequest

				if e := mtMessageParser.Unmarshal(&getDiagnosticsRequest); e != nil {
					log.Printf("cannot unmarshall a Get Diagnostics Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.getDiagnosticsRequestHandler(&getDiagnosticsRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling a Get Diagnostics Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.changeAvailabilityRequestHandler != nil &&
				(mtMessageParser.PayloadMessageIsOfType(SetInoperativeMessageType) ||
					mtMessageParser.PayloadMessageIsOfType(SetOperativeMessageType)) {
				var changeAvailabilityRequest ChangeAvailabilityRequest

				if e := mtMessageParser.Unmarshal(&changeAvailabilityRequest); e != nil {
					log.Printf("cannot unmarshall a Change Availability Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.changeAvailabilityRequestHandler(&changeAvailabilityRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling a Change Availability Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.updateFirmwareRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(UpdateFirmwareMessageType) {
				var updateFirmwareRequest UpdateFirmwareRequest

				if e := mtMessageParser.Unmarshal(&updateFirmwareRequest); e != nil {
					log.Printf("cannot unmarshall an Update Firmware Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.updateFirmwareRequestHandler(&updateFirmwareRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling an Update Firmware Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.removeDiagnosticsRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(RemoveDiagnosticsMessageType) {
				var removeDiagnosticsRequest RemoveDiagnosticsRequest

				if e := mtMessageParser.Unmarshal(&removeDiagnosticsRequest); e != nil {
					log.Printf("cannot unmarshall a Remove Diagnostics Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.removeDiagnosticsRequestHandler(&removeDiagnosticsRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling a Remove Diagnostics Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			if sub.getLogEntryRequestHandler != nil &&
				mtMessageParser.PayloadMessageIsOfType(GetLogEntryMessageType) {
				var getLogEntryRequest GetLogEntryRequest

				if e := mtMessageParser.Unmarshal(&getLogEntryRequest); e != nil {
					log.Printf("cannot unmarshall a Get Log Entry Request from msg %s\n", string(message.Body))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}

				if e := sub.getLogEntryRequestHandler(&getLogEntryRequest); e != nil {
					log.Println(errors.Wrapf(e, "error handling a Get Log Entry Request msg"))
					sub.ch.Nack(message.DeliveryTag, false, true)
					return
				}
			}

			sub.ch.Ack(message.DeliveryTag, false)

		case <-sub.done:
			{
				sub.running = false
				return
			}
		}
	}
}

func (sub *RmqCpsrvSub) Stop() error {
	if !sub.running {
		return ErrNotStarted
	}

	sub.done <- true

	return nil
}
