module rmqcpsrvpubsub

go 1.14

require (
	github.com/pkg/errors v0.8.1
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94
	sycada/masstransitgo v0.0.0
)

replace sycada/masstransitgo v0.0.0 => ../masstransitgo
