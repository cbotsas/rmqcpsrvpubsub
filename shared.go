package rmqcpsrvpubsub

import (
	"time"
)

type ResetRequest struct {
	ChargePointID int
	ResetType     string
}

type ResetRequestHandler func(req *ResetRequest) error

type RemoteStopTransactionRequest struct {
	ChargePointID int
}

type RemoteStopTransactionRequestHandler func(req *RemoteStopTransactionRequest) error

type UnlockConnectorRequest struct {
	ChargePointID int
	ConnectorID   uint8
}

type UnlockConnectorRequestHandler func(req *UnlockConnectorRequest) error

type ClearCacheRequest struct {
	ChargePointID int
}

type ClearCacheRequestHandler func(req *ClearCacheRequest) error

type GetDiagnosticsRequest struct {
	ChargePointID int
	Location      string    `json:"location"`
	StartTime     time.Time `json:"startTime"`
	StopTime      time.Time `json:"stopTime"`
	UserID        int       `json:"userID"`
}

type GetDiagnosticsRequestHandler func(req *GetDiagnosticsRequest) error

type ChangeAvailabilityRequest struct {
	ChargePointID    int
	ConnectorId      uint8 `json:"ConnectorId"`
	AvailabilityType string
}

type ChangeAvailabilityRequestHandler func(req *ChangeAvailabilityRequest) error

type UpdateFirmwareRequest struct {
	ChargePointID int
	Location      string    `json:"location"`
	RetrieveDate  time.Time `json:"retrieveDate"`
}

type UpdateFirmwareRequestHandler func(req *UpdateFirmwareRequest) error

type RemoveDiagnosticsRequest struct {
	ChargePointID int
	DiagFileName  string `json:"Diagfile"`
}

type RemoveDiagnosticsRequestHandler func(req *RemoveDiagnosticsRequest) error

type GetLogEntryRequest struct {
	ChargePointID int
}

type GetLogEntryRequestHandler func(req *GetLogEntryRequest) error

type XFMRequest struct {
	Protocol    string `json:"CpProtocol"`
	RequestType string `json:"requestType"`
	RequestBody string `json:"requestBody"`
}

type XFMResponse struct {
	StatusCode int
	Message    string
	Err        error
}
